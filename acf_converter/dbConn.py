import psycopg2
import ConfigParser
import os


try:
    confPath = '/conf/config.properties'
    configParser = ConfigParser.RawConfigParser()   
    configParser.read(os.path.abspath(os.path.dirname(__file__)) + confPath)
    destinationDatabaseHost = configParser.get('DEFAULT', 'destinationDatabaseHost')
    targetDatabaseUserName = configParser.get('DEFAULT', 'targetDatabaseUserName')
    targetDatabasePassword = configParser.get('DEFAULT', 'targetDatabasePassword')
    targetDatabaseName = configParser.get('DEFAULT', 'targetDatabaseName')

except Exception as e:
       print "excpetion while reading db config parameters" + str(e)

    
class dbConn(object):

  def __init__(self):
      conn_str = "dbname=%s user=%s host=%s password=%s" % (targetDatabaseName,
                                                          targetDatabaseUserName,
                                                          destinationDatabaseHost,
                                                          targetDatabasePassword)
      self.conn = psycopg2.connect(conn_str)
      self.curr =  self.conn.cursor()

  @staticmethod 
  def saveDataintoAcfDb(insert_query,self):
      try:
          self.curr.execute(insert_query)
          self.conn.commit()
          return True
      except Exception as e:
          return False
          print "excpetion while saving into db " , insert_query , str(e)


  @staticmethod 
  def closeConn(self):
      self.conn.close()
      self.curr.close()
