
import json
import datetime
import os
import ConfigParser
import xml.etree.ElementTree as ET
import csv
from dbConn import dbConn
from acfLogger import acfLogger
from commonUtils import commonUtils




try:
    acf_logger = acfLogger()
    commonUtils = commonUtils()
    confPath = '/conf/config.properties'
    configParser = ConfigParser.RawConfigParser()   
    configParser.read(os.path.abspath(os.path.dirname(__file__)) + confPath)
    mappingJsonPath = configParser.get('DEFAULT', 'mappingJsonPath')
    thirdpartyDataSource = configParser.get('DEFAULT', 'thirdpartyDataSource')
    column_name = configParser.get('CUSTOMERMAPPING', 'column_name')
    data_process_length = 0
    if thirdpartyDataSource == 'XML':
       xmlPath = configParser.get('DEFAULT', 'xmlPath')
       root = commonUtils.readXML(xmlPath + 'Companies0.xml')
       data_process_length = len(root.findall('company'))
    elif thirdpartyDataSource == 'CSV':
        csvPath = configParser.get('DEFAULT', 'csvPath')
        data_process_length = commonUtils.csvRecordsCount(csvPath+'customerMapping.csv')

except Exception as e:
       acfLogger.writeErrorLog(acf_logger,"excpetion while reading config parameters in customerMapping" + str(e))


class customerMapping(object):

  def __init__(self):
      self.obj_to_process_count = data_process_length
      self.obj_process_success =  0
      self.obj_process_failure =  0     
      
      


  @staticmethod
  def readCustomerMappingJson(self):    
          try:
              data = commonUtils.readMappingJson(mappingJsonPath+'customerMapping.json')
              self.processXML(data,root,self)
              acfLogger.writeInfoLog(acf_logger,"customer json data to be processed" + str(data))
          except Exception as e:
               acfLogger.writeErrorLog(acf_logger,"excpetion while reading customer mapping json" + str(e))

  @staticmethod
  def readCustomerMappingJson1(self):    
          try:
              data = commonUtils.readMappingJson(mappingJsonPath+'customerMapping.json')
              self.processCSV(data,self)
              acfLogger.writeInfoLog(acf_logger,"customer json data to be processed" + str(data))
          except Exception as e:
                acfLogger.writeErrorLog(acf_logger,"excpetion while reading customer mapping json" + str(e))


  @staticmethod             
  def processXML(data,root,self):
            #insert_queries = []
            try:
               for customer in root:
                   data_count = 0
                   insert_string =  "INSERT INTO" + " " + data[0]['TableName']+"("
                   values1_string = "("
                   for customer1  in customer:
                       for data1 in data:
                        if data1['ThirdPartyFieldName'] is not None:
                              if data1['AmeyoFieldName']!= 'column_name':
                                 if data1['ThirdPartyFieldName'] == customer1.tag:
                                    insert_string = insert_string + data1['AmeyoFieldName']+','
                                    if customer1.text is not None:
                                       values1_string = values1_string + "'" + customer1.text + "'"','
                                    else:
                                        values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                   insert_string = insert_string + 'column_name,'
                   values1_string = values1_string + "'" + column_name + "'"','    
                   insert_string = insert_string.rstrip(',')
                   values1_string = values1_string.rstrip(',')
                   insert_query = insert_string + ") " + "VALUES " + values1_string + ")"
                   acfLogger.writeInfoLog(acf_logger,"insert_query " + insert_query)
                   #insert_queries.append(insert_query)
                   self.saveDataintoAcfDb(insert_query,self)
                   customer1.clear()
                   
            except Exception as e:
                   acfLogger.writeErrorLog(acf_logger,"excpetion while processing customer XML" + str(e))

  @staticmethod             
  def processCSV(data,self):
            #insert_queries = []
            try:
                with open(csvPath+'customerMapping.csv') as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    line_count = 0
                    columns = []
                    for row in csv_reader:
                        if line_count == 0:
                            columns = row
                            line_count = line_count + 1
                        else:
                            insert_string =  "INSERT INTO" + " " + data[0]['TableName']+"("
                            values1_string = "("
                            for data1 in data:
                                column_index = 0
                                for idx,column in enumerate(columns):
                                   if data1['ThirdPartyFieldName'] is not None:
                                      if data1['ThirdPartyFieldName'] == column:
                                          column_index = idx
                                          insert_string = insert_string + data1['AmeyoFieldName']+','
                                          if row[column_index] is not None:
                                             values1_string = values1_string + "'" + row[column_index] + "'"','
                                          else:
                                             values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                            line_count = line_count + 1
                            insert_string = insert_string + 'column_name,'
                            values1_string = values1_string + "'" + column_name + "'"','   
                            insert_string = insert_string.rstrip(',')
                            values1_string = values1_string.rstrip(',')
                            insert_query = insert_string + ") " + "VALUES " + values1_string + ")"
                            acfLogger.writeInfoLog(acf_logger,"insert_query " + insert_query)
                            self.saveDataintoAcfDb(insert_query,self)
            except Exception as e:
                   acfLogger.writeErrorLog(acf_logger,"excpetion while processing customer CSV" + str(e))



  @staticmethod 
  def saveDataintoAcfDb(insert_query,self):
      try:
          db_conn = dbConn()
          if dbConn.saveDataintoAcfDb(insert_query,db_conn) == True:
             self.obj_process_success = self.obj_process_success + 1
          else:
             self.obj_process_failure = self.obj_process_failure + 1
              
      except Exception as e:
             self.obj_process_failure = self.obj_process_failure + 1
             acfLogger.writeErrorLog(acf_logger,"excpetion while saving into db in customerMapping " + insert_query + str(e))
           

