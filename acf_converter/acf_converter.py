
import datetime
import os
import ConfigParser
from dbConn import dbConn
from user_mapping import userMapping
from acfLogger import acfLogger
from queueMapping import queueMapping
from customerMapping import customerMapping
from ticketsMapping import ticketMapping
from messagesMapping import messageMapping


try:
    acf_logger = acfLogger()
    acfLogger.writeSummary("acf converter start time " + str(datetime.datetime.now()))
    confPath = '/conf/config.properties'
    configParser = ConfigParser.RawConfigParser()   
    configParser.read(os.path.abspath(os.path.dirname(__file__)) + confPath)
    mappingJsonPath = configParser.get('DEFAULT', 'mappingJsonPath')
    destinationDatabaseHost = configParser.get('DEFAULT', 'destinationDatabaseHost')
    targetDatabaseUserName = configParser.get('DEFAULT', 'targetDatabaseUserName')
    targetDatabasePassword = configParser.get('DEFAULT', 'targetDatabasePassword')
    targetDatabaseName = configParser.get('DEFAULT', 'targetDatabaseName')
    thirdpartyDataSource = configParser.get('DEFAULT', 'thirdpartyDataSource')

  
    user_mapping = userMapping()
    queue_mapping = queueMapping()
    customer_mapping = customerMapping()
    ticket_mapping = ticketMapping()
    message_mapping = messageMapping()
    if thirdpartyDataSource == 'XML':
       userMapping.readUserMappingJson(user_mapping)
       queueMapping.readQueueMappingJson(queue_mapping)
       customer_mapping.readCustomerMappingJson(customer_mapping)
       ticket_mapping.readTicketMappingJson(ticket_mapping)
       message_mapping.readMessagesMappingJson(message_mapping)
    elif thirdpartyDataSource == 'CSV':
       userMapping.readUserMappingJson1(user_mapping)
       queueMapping.readQueueMappingJson1(queue_mapping)
       customer_mapping.readCustomerMappingJson1(customer_mapping)
       ticket_mapping.readTicketMappingJson1(ticket_mapping)
       message_mapping.readMessagesMappingJson1(message_mapping)

    db_conn = dbConn()
    dbConn.closeConn(db_conn)

        
    
    acfLogger.writeSummary("---------USER CONVERSION---------")
    acfLogger.writeSummary("total objects to process in userMapping "+str(user_mapping.obj_to_process_count))
    acfLogger.writeSummary("total objects successfully saved in userMapping "+ str(user_mapping.obj_process_success))
    acfLogger.writeSummary("total objects to failed to save in userMapping "+ str(user_mapping.obj_process_failure))
    acfLogger.writeSummary("User Conversion Status : Success")
    acfLogger.writeSummary("Target Host : " + str(destinationDatabaseHost))
    acfLogger.writeSummary("Target Database Name : " + str(targetDatabaseName))
    acfLogger.writeSummary("Target Database Username : " + str(targetDatabaseUserName))
    acfLogger.writeSummary("Target Database Password : " + str(targetDatabasePassword))
    acfLogger.writeSummary("User Mapping File Name :" + str(mappingJsonPath+'userMapping.json'))
    

    acfLogger.writeSummary("---------QUEUE CONVERSION---------")
    acfLogger.writeSummary("total objects to process in queueMapping "+ str(queue_mapping.obj_to_process_count))
    acfLogger.writeSummary("total objects successfully saved in queueMapping "+ str(queue_mapping.obj_process_success))
    acfLogger.writeSummary("total objects to failed to save in queueMapping "+ str(queue_mapping.obj_process_failure))
    acfLogger.writeSummary("Queue Conversion Status : Success")
    acfLogger.writeSummary("Target Host : " + str(destinationDatabaseHost))
    acfLogger.writeSummary("Target Database Name : " + str(targetDatabaseName))
    acfLogger.writeSummary("Target Database Username : " + str(targetDatabaseUserName))
    acfLogger.writeSummary("Target Database Password : " + str(targetDatabasePassword))
    acfLogger.writeSummary("Queue Mapping File Name :" + str(mappingJsonPath+'queueMapping.json'))
    


    acfLogger.writeSummary("---------CUSTOMER CONVERSION---------")
    acfLogger.writeSummary("total objects to process in customerMapping "+ str(customer_mapping.obj_to_process_count))
    acfLogger.writeSummary("total objects successfully saved in customerMapping "+ str(customer_mapping.obj_process_success))
    acfLogger.writeSummary("total objects to failed to save in customerMapping "+ str(customer_mapping.obj_process_failure))
    acfLogger.writeSummary("Customer Conversion Status : Success")
    acfLogger.writeSummary("Target Host : " + str(destinationDatabaseHost))
    acfLogger.writeSummary("Target Database Name : " + str(targetDatabaseName))
    acfLogger.writeSummary("Target Database Username : " + str(targetDatabaseUserName))
    acfLogger.writeSummary("Target Database Password : " + str(targetDatabasePassword))
    acfLogger.writeSummary("Customer Mapping File Name :" + str(mappingJsonPath+'customerMapping.json'))
    


    acfLogger.writeSummary("---------TICKET CONVERSION---------")
    acfLogger.writeSummary("total objects to process in ticketsMapping "+ str(ticket_mapping.obj_to_process_count))
    acfLogger.writeSummary("total objects successfully saved in ticketsMapping "+ str(ticket_mapping.obj_process_success))
    acfLogger.writeSummary("total objects to failed to save in ticketsMapping "+ str(ticket_mapping.obj_process_failure))
    acfLogger.writeSummary("Ticket Conversion Status : Success")
    acfLogger.writeSummary("Target Host : " + str(destinationDatabaseHost))
    acfLogger.writeSummary("Target Database Name : " + str(targetDatabaseName))
    acfLogger.writeSummary("Target Database Username : " + str(targetDatabaseUserName))
    acfLogger.writeSummary("Target Database Password : " + str(targetDatabasePassword))
    acfLogger.writeSummary("Ticket Mapping File Name :" + str(mappingJsonPath+'ticketsMapping.json'))


    acfLogger.writeSummary("---------TICKET MESSAGE CONVERSION---------")
    acfLogger.writeSummary("total objects to process in messageMapping "+ str(message_mapping.obj_to_process_count))
    acfLogger.writeSummary("total objects successfully saved in messageMapping "+ str(message_mapping.obj_process_success))
    acfLogger.writeSummary("total objects to failed to save in messageMapping "+ str(message_mapping.obj_process_failure))
    acfLogger.writeSummary("Message Conversion Status : Success")
    acfLogger.writeSummary("Message Host : " + str(destinationDatabaseHost))
    acfLogger.writeSummary("Message Database Name : " + str(targetDatabaseName))
    acfLogger.writeSummary("Message Database Username : " + str(targetDatabaseUserName))
    acfLogger.writeSummary("Message Database Password : " + str(targetDatabasePassword))
    acfLogger.writeSummary("Message Mapping File Name :" + str(mappingJsonPath+'ticketMessagesMapping.json'))
    


    acfLogger.writeSummary("acf converter end time " + str(datetime.datetime.now()))      
    
except Exception as e:
        acfLogger.writeErrorLog(acf_logger,"excpetion in acfConverter" + str(e))

