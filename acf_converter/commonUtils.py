
import json
import xml.etree.ElementTree as ET
import csv
from acfLogger import acfLogger


class commonUtils(object):

  def __init__(self):
      self.name = "customUtils" 
      
      


  @staticmethod
  def readMappingJson(jsonPath):    
          try:
              with open(jsonPath) as mapping_file:
                   mapping_data = mapping_file.read()
                   data = json.loads(mapping_data)
                   return data
          except Exception as e:
                return []
                acfLogger.writeErrorLog(acf_logger,"excpetion while reading mapping json" + str(e))


  @staticmethod
  def csvRecordsCount(csvPath):    
          try:
              rows_count = -1
              with open(csvPath) as csv_file:
                   csv_reader = csv.reader(csv_file, delimiter=',')
                   for row in csv_reader:
                       rows_count = rows_count + 1
              return rows_count
          except Exception as e:
                return 0
                acfLogger.writeErrorLog(acf_logger,"excpetion while counting csv records" + str(e))


  @staticmethod
  def readXML(xmlFilePath):    
          try:
              tree = ET.parse(xmlFilePath)
              root = tree.getroot()
              return root
          except Exception as e:
                return ""
                acfLogger.writeErrorLog(acf_logger,"excpetion while reading mapping json" + str(e))
                

  @staticmethod
  def elementInListOrNot(givenList,element):    
          try:
              
              if givenList.count(element) > 0:
                 return True
              else:
                 return False
          except Exception as e:
                return False
                acfLogger.writeErrorLog(acf_logger,"excpetion in elementInListOrNot method" + str(e))

  @staticmethod
  def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist

                
     
