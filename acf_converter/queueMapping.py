
import os
import ConfigParser
import csv
from dbConn import dbConn
from acfLogger import acfLogger
from commonUtils import commonUtils



try:
    acf_logger = acfLogger()
    commonUtils = commonUtils()
    confPath = '/conf/config.properties'
    configParser = ConfigParser.RawConfigParser()   
    configParser.read(os.path.abspath(os.path.dirname(__file__)) + confPath)
    mappingJsonPath = configParser.get('DEFAULT', 'mappingJsonPath')
    thirdpartyDataSource = configParser.get('DEFAULT', 'thirdpartyDataSource')
    data_process_length = 0
    if thirdpartyDataSource == 'XML':
       xmlPath = configParser.get('DEFAULT', 'xmlPath')
       root = commonUtils.readXML(xmlPath + 'Groups.xml')
       data_process_length = len(root.findall('group'))
    elif thirdpartyDataSource == 'CSV':
        csvPath = configParser.get('DEFAULT', 'csvPath')
        data_process_length = commonUtils.csvRecordsCount(csvPath+'queueMapping.csv')

except Exception as e:
       acfLogger.writeErrorLog(acf_logger,"excpetion while reading config parameters in queueMapping" + str(e))


class queueMapping(object):

  def __init__(self):
      self.obj_to_process_count = data_process_length
      self.obj_process_success =  0
      self.obj_process_failure =  0     
      
      


  @staticmethod
  def readQueueMappingJson(self):    
          try:
              data = commonUtils.readMappingJson(mappingJsonPath+'queueMapping.json')
              queueMapping.processXML(data,root,self)
              acfLogger.writeInfoLog(acf_logger,"queue json data to be processed" + str(data))
          except Exception as e:
               acfLogger.writeErrorLog(acf_logger,"excpetion while reading queue mapping json" + str(e))

  @staticmethod
  def readQueueMappingJson1(self):    
          try:
              data = commonUtils.readMappingJson(mappingJsonPath+'queueMapping.json')
              queueMapping.processCSV(data,self)
              acfLogger.writeInfoLog(acf_logger,"queue json data to be processed" + str(data))
          except Exception as e:
                acfLogger.writeErrorLog(acf_logger,"excpetion while reading queue mapping json" + str(e))


  @staticmethod             
  def processXML(data,root,self):
            #insert_queries = []
            try:
               for queue in root:
                   data_count = 0
                   insert_string =  "INSERT INTO" + " " + data[0]['TableName']+"("
                   values1_string = "("
                   for queue1  in queue:
                       for data1 in data:
                        if data1['ThirdPartyFieldName'] is not None:
                              if data1['ThirdPartyFieldName'] == queue1.tag:
                                  insert_string = insert_string + data1['AmeyoFieldName']+','
                                  if queue1.text is not None:
                                     values1_string = values1_string + "'" + queue1.text + "'"','
                                  else:
                                    values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                   insert_string = insert_string.rstrip(',')
                   values1_string = values1_string.rstrip(',')
                   insert_query = insert_string + ") " + "VALUES " + values1_string + ")"
                   acfLogger.writeInfoLog(acf_logger,"insert_query " + insert_query)
                   #insert_queries.append(insert_query)
                   queueMapping.saveDataintoAcfDb(insert_query,self)
                   queue1.clear()
                   
            except Exception as e:
                   acfLogger.writeErrorLog(acf_logger,"excpetion while processing queue XML" + str(e))

  @staticmethod             
  def processCSV(data,self):
            #insert_queries = []
            try:
                with open(csvPath+'queueMapping.csv') as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    line_count = 0
                    columns = []
                    for row in csv_reader:
                        if line_count == 0:
                            columns = row
                            line_count = line_count + 1
                        else:
                            insert_string =  "INSERT INTO" + " " + data[0]['TableName']+"("
                            values1_string = "("
                            for data1 in data:
                                column_index = 0
                                for idx,column in enumerate(columns):
                                   if data1['ThirdPartyFieldName'] is not None:
                                      if data1['ThirdPartyFieldName'] == column:
                                          column_index = idx
                                          insert_string = insert_string + data1['AmeyoFieldName']+','
                                          if row[column_index] is not None:
                                             values1_string = values1_string + "'" + row[column_index] + "'"','
                                          else:
                                             values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                            line_count = line_count + 1
                            insert_string = insert_string.rstrip(',')
                            values1_string = values1_string.rstrip(',')
                            insert_query = insert_string + ") " + "VALUES " + values1_string + ")"
                            acfLogger.writeInfoLog(acf_logger,"insert_query " + insert_query)
                            queueMapping.saveDataintoAcfDb(insert_query,self)
            except Exception as e:
                   acfLogger.writeErrorLog(acf_logger,"excpetion while processing queue CSV" + str(e))

  @staticmethod 
  def saveDataintoAcfDb(insert_query,self):
      try:
          db_conn = dbConn()
          if dbConn.saveDataintoAcfDb(insert_query,db_conn) == True:
             self.obj_process_success = self.obj_process_success + 1
          else:
             self.obj_process_failure = self.obj_process_failure + 1
              
      except Exception as e:
             self.obj_process_failure = self.obj_process_failure + 1
             acfLogger.writeErrorLog(acf_logger,"excpetion while saving into db in queueMapping " + insert_query + str(e))
           

