import json
import logging
import datetime
import os
import ConfigParser
import xml.etree.ElementTree as ET
import csv
from dbConn import dbConn
from acfLogger import acfLogger
from commonUtils import commonUtils



try:
    acf_logger = acfLogger()
    commonUtils = commonUtils()
    confPath = '/conf/config.properties'
    configParser = ConfigParser.RawConfigParser()   
    configParser.read(os.path.abspath(os.path.dirname(__file__)) + confPath)
    mappingJsonPath = configParser.get('DEFAULT', 'mappingJsonPath')
    thirdpartyDataSource = configParser.get('DEFAULT', 'thirdpartyDataSource')
    data_process_length = 0
    if thirdpartyDataSource == 'XML':
       xmlPath = configParser.get('DEFAULT', 'xmlPath')
       root = commonUtils.readXML(xmlPath + 'Tickets0.xml')
       data_process_length = len(root.findall('helpdesk-ticket'))
    elif thirdpartyDataSource == 'CSV':
        csvPath = configParser.get('DEFAULT', 'csvPath')
        data_process_length = commonUtils.csvRecordsCount(csvPath+'messagesMapping.csv')
    translation_strings = ['incoming']

except Exception as e:
       acfLogger.writeErrorLog(acf_logger,"excpetion while reading config parameters in messages Mapping" + str(e))


class messageMapping(object):

  def __init__(self):
      self.obj_to_process_count = data_process_length
      self.obj_process_success =  0
      self.obj_process_failure =  0     
      
      


  @staticmethod
  def readMessagesMappingJson(self):    
          try:
              data = commonUtils.readMappingJson(mappingJsonPath+'ticketMessagesMapping.json')
              messageMapping.processXML(data,root,self)
              acfLogger.writeInfoLog(acf_logger,"messages json data to be processed" + str(data))
          except Exception as e:
               acfLogger.writeErrorLog(acf_logger,"excpetion while reading messages mapping json" + str(e))

  @staticmethod
  def readMessagesMappingJson1(self):    
          try:
              data = commonUtils.readMappingJson(mappingJsonPath+'ticketMessagesMapping.json')
              self.processCSV(data,self)
              acfLogger.writeInfoLog(acf_logger,"messages json data to be processed" + str(data))
          except Exception as e:
                acfLogger.writeErrorLog(acf_logger,"excpetion while reading messages mapping json" + str(e))




  @staticmethod             
  def processXML(data,root,self):
            #insert_queries = []
            translations = commonUtils.readMappingJson(os.path.abspath(os.path.dirname(__file__))+'/translations/messageTranslation.json')
            try:
               for message in root:
                   data_count = 0
                   for message1  in message:
                       if message1.tag == 'notes':
                           for message2 in message1:
                               insert_string =  "INSERT INTO" + " " + data[0]['TableName']+"("
                               values1_string = "("
                               for message3 in message2:
                                   for data1 in data:
                                       if data1['ThirdPartyFieldName'] is not None:
                                          if data1['ThirdPartyFieldName'] == message3.tag:
                                             insert_string = insert_string + data1['AmeyoFieldName']+','
                                             if commonUtils.elementInListOrNot(translation_strings,data1['ThirdPartyFieldName']) == True:
                                                if message3.text is not None:
                                                   tranlated_string = ''
                                                   for translation1 in translations:
                                                       try:
                                                           tranlated_string = translation1[data1['ThirdPartyFieldName']][message3.text]
                                                       except Exception as e:
                                                              tranlated_string = data1['defaultValue']
                                                   values1_string = values1_string + "'" + tranlated_string + "'"','   
                                                else:
                                                    values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                                             else:
                                                if message3.text is not None:
                                                   values1_string = values1_string + "'" + message3.text + "'"','
                                                else:
                                                   values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                               insert_string = insert_string.rstrip(',')
                               values1_string = values1_string.rstrip(',')
                               insert_query = insert_string + ") " + "VALUES " + values1_string + ")"
                               acfLogger.writeInfoLog(acf_logger,"insert_query " + insert_query)
                               print "insert_query " + insert_query
                               #insert_queries.append(insert_query)
                               messageMapping.saveDataintoAcfDb(insert_query,self)
                   message1.clear()
                   
            except Exception as e:
                   acfLogger.writeErrorLog(acf_logger,"excpetion while processing messages XML" + str(e))

  @staticmethod             
  def processCSV(data,self):
            #insert_queries = []
            translations = commonUtils.readMappingJson(os.path.abspath(os.path.dirname(__file__))+'/translations/messageTraslation.json')
            try:
                with open(csvPath+'messagesMapping.csv') as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    line_count = 0
                    columns = []
                    for row in csv_reader:
                        if line_count == 0:
                            columns = row
                            line_count = line_count + 1
                        else:
                            insert_string =  "INSERT INTO" + " " + data[0]['TableName']+"("
                            values1_string = "("
                            for data1 in data:
                                column_index = 0
                                for idx,column in enumerate(columns):
                                   if data1['ThirdPartyFieldName'] is not None:
                                      if data1['ThirdPartyFieldName'] == column:
                                          column_index = idx
                                          insert_string = insert_string + data1['AmeyoFieldName']+','
                                          if commonUtils.elementInListOrNot(translation_strings,data1['ThirdPartyFieldName']) == True:
                                             if row[column_index] is not None:
                                                tranlated_string = ''
                                                for translation1 in translations:
                                                     try:
                                                         tranlated_string = translation1[data1['ThirdPartyFieldName']][row[column_index]]
                                                     except Exception as e:
                                                         tranlated_string = data1['defaultValue']
                                                values1_string = values1_string + "'" + tranlated_string + "'"','   
                                             else:
                                                 values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                                          else:
                                              if row[column_index] is not None:
                                                 values1_string = values1_string + "'" + row[column_index] + "'"','
                                              else:
                                                  values1_string = values1_string + "'" + data1['defaultValue'] + "'" + ','
                            line_count = line_count + 1
                            insert_string = insert_string.rstrip(',')
                            values1_string = values1_string.rstrip(',')
                            insert_query = insert_string + ") " + "VALUES " + values1_string + ")"
                            acfLogger.writeInfoLog(acf_logger,"insert_query " + insert_query)
                            ticketMapping.saveDataintoAcfDb(insert_query,self)
            except Exception as e:
                   acfLogger.writeErrorLog(acf_logger,"excpetion while processing messages CSV" + str(e))



  @staticmethod 
  def saveDataintoAcfDb(insert_query,self):
      try:
          db_conn = dbConn()
          if dbConn.saveDataintoAcfDb(insert_query,db_conn) == True:
             self.obj_process_success = self.obj_process_success + 1
          else:
             self.obj_process_failure = self.obj_process_failure + 1
              
      except Exception as e:
             self.obj_process_failure = self.obj_process_failure + 1
             acfLogger.writeErrorLog(acf_logger,"excpetion while saving into db in messagesMapping " + insert_query + str(e))
           

