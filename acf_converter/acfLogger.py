
import datetime
import logging
import os

    
class acfLogger(object):

  def __init__(self):
      self.logger = logging.getLogger(__name__)

  @staticmethod 
  def setInfoLog(self):
      try:
          info_handler = logging.FileHandler(os.path.abspath(os.path.dirname(__file__)) +
                                    '/logs/acf_converter_info.log')
          info_handler.setLevel(logging.WARNING)
          info_format = logging.Formatter('%(asctime)s - %(name)s  - %(message)s')
          info_handler.setFormatter(info_format)
          self.logger.addHandler(info_handler)
      except Exception as e:
          print "excpetion while setting info log " ,  str(e)


  @staticmethod 
  def setErrorLog(self):
      try:
          error_handler = logging.FileHandler(os.path.abspath(os.path.dirname(__file__)) +
                                    '/logs/acf_converter_error.log')
          error_handler.setLevel(logging.ERROR)
          error_format = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
          error_handler.setFormatter(error_format)
          self.logger.addHandler(error_handler)
      except Exception as e:
          print "excpetion while setting error log " ,  str(e)

  
  @staticmethod 
  def writeInfoLog(self,data):
      try:
          self.logger.warning(data)
      except Exception as e:
          print "excpetion while writing info log " ,  str(e)



  @staticmethod 
  def writeErrorLog(self,data):
      try:
          self.logger.error(data)
      except Exception as e:
          print "excpetion while writing error log " , str(e)

  @staticmethod 
  def writeSummary(data):
      try:
           summaryPath = '/summary/acf_convertor_summary.txt'
           f = open(os.path.abspath(os.path.dirname(__file__)) + summaryPath,"a")
           f.write("\n")
           f.write(str(datetime.datetime.now()) + " " + data + "\n")
      except Exception as e:
             print "excpetion while writing to summary" + str(e) 



self = acfLogger()
acfLogger.setInfoLog(self)
acfLogger.setErrorLog(self)
